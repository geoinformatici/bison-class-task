%{
#include <stdio.h>
#include <string.h>

int yylex();
void yyerror(const char *);

extern FILE *yyin;
extern char *yytext;

#define STACK_SIZE 10
struct pair {
  char *name;
  int value;
};
struct pair env[STACK_SIZE];
int pos = 0;
int oldpos;
int find_variable(char *name) {
  int i;
  for (i=0; i<pos && i<STACK_SIZE; i++) {
    if (!strcmp(env[i].name, name)) {
      return i;
    }
  }
  return -1;
}
%}
%union {
  int value;
  char *svalue;
}

%token <value> NUMBER
%token <svalue> ID
%token LET
%token LEX_ERR
%token STACKFULL_ERR
%token UNDEFINED_ERR
%token REDEFINED_ERR

%type <value> exp

%precedence '='
%left '+' '-'
%left '*' '/'
%right OP_UMINUS

%define parse.error verbose
%start program
%%
program : exp { printf("Result: %d\n", $1); }
        | program ';' exp { printf("Result: %d\n", $3); }
        ;

exp : exp '+' exp { $$ = $1 + $3; }
    | exp '-' exp { $$ = $1 - $3; }
    | '-' %prec OP_UMINUS exp { $$ = -$2; }
    | NUMBER { $$ = $1; }
    | exp '*' exp { $$ = $1 * $3; }
    | exp '/' exp { $$ = $1 / $3; }
    | ID {
        int index = find_variable($1);
        if (index != -1) $$ = env[index].value;
        else yychar = UNDEFINED_ERR;
      }
    | '?' { scanf("%d", &$$); }
    | { oldpos = pos; } '[' LET assignment_list ']' '{' exp '}' { $$ = $7; pos = oldpos;}
    ;

assignment_list : assignment
                | assignment ',' assignment_list

assignment : ID '=' exp {
               if (pos == STACK_SIZE) yychar = STACKFULL_ERR;
               int index = find_variable($1);
               if (index != -1) yychar = REDEFINED_ERR;
               env[pos].name = $1;
               env[pos].value = $3;
               pos++;
             }
%%
void yyerror(const char *msg) {
  switch (yychar) {
    case LEX_ERR:
      fprintf(stderr, "Lexical error: unkown token '%s'\n", yytext);
      break;
    case STACKFULL_ERR:
      fprintf(stderr, "Too many variables defined\n");
      break;
    case UNDEFINED_ERR:
      fprintf(stderr, "Variable not yet defined\n");
      break;
    case REDEFINED_ERR:
      fprintf(stderr, "Variable already defined\n");
      break;
    default:
      fprintf(stderr, "%s\n", msg);
  }
}
int main(int argc, char **argv) {
  if (argc < 2) {
    fprintf(stderr, "Missing input file!\n");
  }
  yyin = fopen(argv[1], "r");
  if (yyparse()) {
    fprintf(stderr, "Unable to parse the input!\n");
    return 1;
  }

  fclose(yyin);
  return 0;
}
