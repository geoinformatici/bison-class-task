%{
#include "exp.parser.h"
#include <stdio.h>
#include <string.h>
%}
DIGIT [0-9]
ID [A-Za-z]+
NL \n
%option noyywrap
%%
[ \t\r\n]+   {}
"let"    { return LET; }
{ID}     { yylval.svalue = strdup(yytext); return ID; }
{DIGIT}+ { yylval.value = atoi(yytext); return NUMBER; }
"+"      { return '+'; }
"-"      { return '-'; }
"*"      { return '*'; }
"/"      { return '/'; }
"="      { return '='; }
","      { return ','; }
"?"      { return '?'; }
"["      { return '['; }
"]"      { return ']'; }
"{"      { return '{'; }
"}"      { return '}'; }
";"      { return ';'; }
.        { return LEX_ERR; }
%%
